# README

I just deleted the default README.md text and replaced it with this message. Now I am going to save the file using the text editor, but the work is not done with git!

Saving the file simply writes the new text to your hard disk. We need to tell git that we want to preserve these changes for the project (making a "commit"), then send them back to the repository on GitLab so the rest of our team can see them (this is "pushing to origin").

Make changes with the web IDE!
